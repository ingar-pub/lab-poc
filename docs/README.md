---
home: true
title: Proyectos INGAR
heroImage: http://www.ingar.santafe-conicet.gov.ar/wp-content/uploads/2015/05/logo_ingar_365x50-2.png
heroHeight: 80
actions:
  - text: Plataforma PUE
    link: /plataforma/
    type: primary
  - text: App Starplastic
    link: /starplastic/
    type: secondary
  - text: App Green PtX
    link: /green_ptx/
    type: secondary
features:
- title: Apps de productividad
  details: Plataforma de Apps de productividad avanzada mediante modelado matemático
- title: Industria 4.0
  details: Sensorización industrial y Planificación de la producción
- title: Soluciones de Energía
  details: Nivelado de hidrógeno para parques eólicos
footer: Hecho en Santa Fe, Argentina | Copyright © 2024 INGAR
---