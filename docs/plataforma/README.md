# INGAR PUE Platform

La plataforma PUE es una plataforma de software para ejecutar como **Apps** modelos matemáticos de optimización industrial.

## Implementaciones

* App1 - Problema de Corte (Cutting stock - GAMS - MILP): Corte de productos de papel de diferentes tamaños de un gran rollo de papel crudo, para satisfacer pedidos. El objetivo es minimizar el número requerido de rollos de papel.
* App2 - Operaciones de Producción y Distribución (MILP - Gurobi): Resolución de problema de batching y scheduling de la producción y distribución en una instalación batch monoetapa, a fin de minimizar el costo total de las operaciones de producción y distribución.
* App3 - Optimización de logística de distribución (MILP - Gurobi): Generación de ruteo para distribuir materias primas desde los frentes de cosecha a las plantas con flota de camiones, minimizando el costo y satisfaciendo la demanda.
* App4 - Planificación de secadero de maderas (GAMS - MILP): Generación de ruteo para distribuir materias primas desde los frentes de cosecha a las plantas con flota de camiones, minimizando el costo y satisfaciendo la demanda.
* App5 - Starplastic - Monitoreo y Planificación de la Producción: Planificación de la producción en planta industrial y sensorización en tiempo real de planta.
* App6 - Costo Nivelado H2: Visualización de mapas y generación de cálculos de costos para la locación de plantas basados en cálculos climáticos.
