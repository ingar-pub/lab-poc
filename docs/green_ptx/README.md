---
lang: es-AR
home: true
heroText: Green PtX
tagline: Costo de Nivelado PtX
heroImage: /images/logos.png
heroHeight: 80
actions:
  - text: Acceder a la App
    link: https://ingarue.santafe-conicet.gov.ar/app6
    type: primary
features:
- title: Información climática por horas
  details: Análisis detallado de generación por hora
- title: Entrega del producto
  details: Análisis completo de extremo a extremo
- title: Uniforme / No uniforme
  details: Generación diferenciada bajo condiciones climáticas no uniformes.
- title: Anual
  details: Analiza todo el año en detalle
- title: Completitud económica
  details: Analizar todos los aspectos económicos involucrados
- title: Almacenamiento
  details: Análisis de almacenamiento de energía (gas hidrógeno comprimido)
lastUpdated: false
contributors: false
footer: Hecho en Santa Fe, Argentina | Copyright © 2024 INGAR
---
# Green Power-to-X App

Bienvenido a Green PtX, la plataforma todo-en-uno para analizar inversiones y retornos en el ámbito de la energía verde. Con Green PtX, podrá calcular sin esfuerzo la rentabilidad potencial y el impacto ambiental de sus inversiones en proyectos de energía renovable.

## Costo de Nivelado PtX

Nuestro completo conjunto de herramientas le permite ingresar varios parámetros, como inversión inicial, producción de energía proyectada, costos operativos e incentivos gubernamentales para generar pronósticos precisos de sus retornos a lo largo del tiempo.

## Artículos Científicos que respaldan nuestro trabajo

Para obtener más información sobre el proceso de cálculo, consulte la publicación <a href="https://doi.org/10.1016/j.ijhydene.2023.12.052" target="_blank">Green hydrogen levelized cost assessment from wind energy in Argentina with dispatch constraints</a>.

::: tip INFO
Ya sea que esté considerando la energía solar, eólica, hidroeléctrica u otras formas de energía verde, Green PtX le brinda la información y el análisis basado en datos que necesita para tomar decisiones de inversión informadas que se alineen con sus metas financieras y objetivos de sostenibilidad. Únase a Green PtX hoy y embárquese en un viaje hacia un futuro más verde y próspero.
:::

## Herramientas disponibles y en desarrollo

### 1. LCOH gas – Levelized cost of Hydrogen

Nuestra herramienta avanzada le permite optimizar con precisión el costo nivelado de la producción de gas hidrógeno, evaluar las inversiones necesarias y evaluar los gastos operativos. Obtenga información más detallada sobre la eficiencia de su proyecto analizando los factores de capacidad y el ciclo de reemplazo de los electrolizadores. Simplemente ingrese la ubicación específica de su instalación y el CAPEX y OPEX de los componentes de su sistema, y ​​deje que nuestra herramienta haga el resto. Agilice su proceso de toma de decisiones y mejore el éxito de sus iniciativas de hidrógeno verde.

#### 1.1. LCOH liq – Levelized cost of liquid hydrogen

Lleve su análisis de producción de hidrógeno al siguiente nivel con nuestra herramienta diseñada específicamente para optimizar el costo nivelado del hidrógeno líquido. Evalúe las inversiones necesarias, los costos operativos y optimice la eficiencia general de su proyecto. Al ingresar detalles clave como la ubicación específica de su instalación y el CAPEX y OPEX de los componentes de su sistema, obtendrá información valiosa sobre la economía de la producción de hidrógeno líquido. Simplifique su proceso de toma de decisiones y garantice el éxito financiero de sus proyectos de hidrógeno líquido.

#### 1.2 LCOH unif – Levelized cost of hydrogen (gas or liquid) at uniform delivery

Garantice la coherencia y la fiabilidad con nuestra herramienta que optimiza el coste nivelado del hidrógeno (ya sea gas o líquido) suministrado de forma uniforme. Esta función le permite evaluar con precisión las implicaciones de costes que supone mantener un suministro constante y uniforme, teniendo en cuenta las inversiones necesarias, los gastos operativos y los ciclos de sustitución. El sistema también puede compensar los déficits de energía renovable aprovechando una conexión a la red limitada o una batería de iones de litio de respaldo. Introduzca la ubicación específica, el CAPEX y el OPEX de los componentes de su sistema y reciba cálculos precisos que respalden la toma de decisiones informadas. Logre un suministro constante y optimice el rendimiento económico de sus proyectos de hidrógeno.

### 2. LCOA – Levelized cost of Ammonia

Optimice sus proyectos de producción de amoníaco con nuestra herramienta integral, diseñada específicamente para calcular el costo nivelado del amoníaco. Esta herramienta cubre todo el proceso de producción, incluida la generación de nitrógeno a través de unidades de separación de aire (ASU) y el almacenamiento intermedio de gases de hidrógeno y nitrógeno. La planta de producción de Haber-Bosch opera con una capacidad de reducción y aumento/reducción determinadas, para el funcionamiento adecuado de la planta y garantizar la flexibilidad y la eficiencia. Además, el sistema puede compensar los déficits de energía renovable mediante el uso de una conexión a la red limitada o una batería de respaldo de iones de litio. Ingrese la ubicación de su instalación, la reducción, las rampas, el CAPEX y el OPEX para obtener un análisis de costos preciso, lo que le permitirá optimizar la entrega de amoníaco, ya sea para la distribución por tuberías u otras aplicaciones. Tome decisiones informadas e impulse el éxito de sus proyectos de amoníaco con confianza.

#### 2.1. LCOA – Levelized cost of Ammonia at uniform delivery

Optimice sus operaciones de producción y exportación de amoníaco con nuestra sofisticada herramienta diseñada para optimizar el costo nivelado del amoníaco entregado de manera uniforme. Esta característica es ideal para escenarios como el despacho de amoníaco en barcos transoceánicos para exportación. Nuestra herramienta ofrece un análisis integral al considerar las variaciones en la producción y la entrega a lo largo del año, incluida la producción de nitrógeno a través de unidades de separación de aire (ASU) y el almacenamiento intermedio de gases de hidrógeno y nitrógeno. La planta de producción de Haber-Bosch opera con una capacidad de reducción y aumento/reducción determinadas, para la operación adecuada de la planta, lo que permite ajustes flexibles para cumplir con los requisitos de entrega constantes. Gestione las fluctuaciones de energía renovable con una conexión a la red limitada o una batería de respaldo de iones de litio para una operación continua. Al ingresar la ubicación de su instalación, la reducción, las rampas, el CAPEX y el OPEX, obtendrá cálculos precisos de los costos asociados con la entrega uniforme de amoníaco, lo que garantiza operaciones de exportación confiables y rentables. Optimice su cadena de suministro y mejore la rentabilidad de sus empresas de exportación de amoníaco con nuestro análisis de costos detallado.

### 3. LCOMeOH – Levelized cost of Methanol

Minimice el costo nivelado del metanol verde con nuestra herramienta avanzada. Esta solución incorpora una configuración de producción sofisticada, que incluye una planta de producción de hidrógeno, captura de CO2 de una instalación vecina, licuefacción de CO2 y almacenamiento intermedio tanto para hidrógeno como para CO2, lo que garantiza un proceso de síntesis estable. La unidad de síntesis de metanol funciona con una capacidad de reducción determinada y una respuesta dinámica rápida y está equipada con componentes esenciales como compresores multietapa, intercambiadores de calor, evaporadores, un reactor de síntesis y una columna de destilación. Si bien el uso de un tanque de almacenamiento de metanol crudo no está modelado específicamente, su influencia en el proceso de destilación se considera en el análisis de costos. Al ingresar la ubicación de su instalación, la reducción, las rampas, el CAPEX y el OPEX, recibirá información detallada sobre los costos, lo que lo ayudará a optimizar el rendimiento financiero de su producción de metanol verde e impulsar el éxito de sus iniciativas de energía sustentable.

#### 3.1. LCOMeOH – Levelized cost of Methanol at uniform delivery

Garantice la consistencia y la confiabilidad en su suministro de metanol ecológico con nuestra herramienta avanzada, diseñada para optimizar el costo nivelado del metanol entregado de manera uniforme. Ideal para aplicaciones como el envío a granel para exportación o distribución a gran escala, esta herramienta tiene en cuenta una configuración de producción integral, que incluye la producción de hidrógeno, la captura y licuefacción de CO2 y el almacenamiento intermedio tanto de hidrógeno como de CO2. Si bien no está modelada específicamente, la herramienta también considera el uso de un tanque de almacenamiento de metanol crudo para respaldar el proceso de destilación. Al ingresar la ubicación de su instalación, la reducción de la demanda, las rampas, el CAPEX y el OPEX, obtendrá información precisa sobre los costos para una entrega uniforme de metanol, maximizando la eficiencia financiera y logística de sus proyectos de energía sustentable.

### 4. LCOeK – Levelized cost of e-Kerosene

Optimice su producción de e-queroseno con nuestra herramienta avanzada, diseñada para calcular el costo nivelado del e-queroseno. Esta herramienta incorpora una configuración de producción detallada, que incluye la producción de hidrógeno, la captura de CO2 de una instalación vecina, la licuefacción de CO2 y el almacenamiento intermedio tanto de hidrógeno como de CO2 para garantizar una operación estable y económica del proceso Fischer-Tropsch (FT). El proceso FT ofrece una flexibilidad moderada, lo que permite una reducción determinada de la carga del reactor de su capacidad nominal, lo que requiere una capacidad de almacenamiento adecuada o energía de la red para mantener los niveles operativos. La herramienta también considera la necesidad de un tanque de almacenamiento de e-queroseno crudo aguas abajo del reactor para garantizar una operación constante en el sector de refinación y destilación. También evalúa la capacidad de almacenamiento necesaria para que el e-queroseno cumpla con las condiciones de despacho. Al ingresar la ubicación de su instalación, la reducción de la demanda, las rampas, el CAPEX y el OPEX, recibirá información precisa sobre los costos, lo que le permitirá equilibrar los requisitos de energía máxima y las necesidades de almacenamiento, optimizando el rendimiento financiero y operativo de su proceso de producción de e-queroseno.

#### 4.1. LCOeK – Levelized cost of e-Kerosene at uniform delivery

Asegúrese de que el suministro de queroseno electrónico sea constante y confiable con nuestra herramienta avanzada, diseñada para optimizar el costo nivelado del queroseno electrónico entregado de manera uniforme. Esta herramienta es ideal para aplicaciones que requieren una entrega constante, como la distribución o exportación a gran escala. Incorpora una configuración de producción detallada que incluye la producción de hidrógeno, el CO2 tratado y el almacenamiento intermedio, fundamentales para el funcionamiento estable del proceso Fischer-Tropsch (FT). El proceso FT ofrece una flexibilidad moderada, lo que permite una reducción en la carga del reactor para mantener los niveles operativos. Nuestra herramienta garantiza que su instalación pueda mantener estos niveles con una capacidad de almacenamiento adecuada o con el respaldo de la energía de la red. También considera la necesidad de un tanque de almacenamiento de queroseno electrónico crudo aguas abajo del reactor para respaldar el funcionamiento continuo en los sectores de refinación y destilación. La herramienta evalúa las capacidades de almacenamiento necesarias para que el queroseno electrónico cumpla con las condiciones de entrega uniforme. Al ingresar la ubicación de su instalación, la reducción de la demanda, las rampas, el CAPEX y el OPEX, recibirá información precisa sobre los costos para una entrega uniforme de queroseno electrónico, lo que optimizará el desempeño financiero y logístico de sus proyectos de energía sustentable.

## Quienes somos

Nuestro equipo está formado por un grupo diverso de ingenieros y científicos, unidos por una pasión compartida por promover soluciones de energía verde. Con experiencia que abarca sistemas de energía renovable, ciencia ambiental y tecnología sustentable, nos dedicamos a impulsar la innovación que minimiza el impacto ambiental y maximiza la eficiencia y la confiabilidad. Nuestro enfoque multidisciplinario nos permite abordar desafíos complejos en el sector de la energía renovable, desde energía solar y eólica hasta almacenamiento de energía y tecnologías de redes inteligentes. Estamos comprometidos con el desarrollo de soluciones sustentables que no solo satisfagan las necesidades energéticas actuales, sino que también preparen el camino para un futuro más limpio y resiliente.

![img](./logos_90.png)
