import { viteBundler } from '@vuepress/bundler-vite'
import { defaultTheme } from '@vuepress/theme-default'
import { defineUserConfig } from 'vuepress'

export default defineUserConfig({
  lang: 'es-AR',
  title: 'Proyectos INGAR',
  description: 'Proyectos desarrollados en el Instituto INGAR',
  base: '/projects/',
  dest: 'public',
  bundler: viteBundler(),
  locales: {
    // The key is the path for the locale to be nested under.
    // As a special case, the default locale can use '/' as its path.
    '/': {
      lang: 'es-AR',
      title: 'Proyectos INGAR',
      description: 'Proyectos desarrollados en el Instituto INGAR',
    },
    '/en/': {
      lang: 'en-US',
      title: 'INGAR Projects',
      description: 'Projects developed at INGAR Institute',
    },
  },
  theme: defaultTheme({
    // default theme config
    locales: {
      '/': {
        selectLanguageName: 'Español',
      },
      '/en/': {
        selectLanguageName: 'English',
      },
    },
    sidebar: {
      '/plataforma/': [
        {
          text: 'PUE',
          children: [
            { text: 'Tecnología', link: 'tecnologia'}
          ]
        }
      ],
      '/starplastic/': [
        {
          text: 'Starplastic',
          children: [
            { text: 'Guía del usuario', link: 'MANUAL'},
            { text: 'Guía del admin', link: 'MANUAL_SYSADMIN'},
            { text: 'Release notes', link: 'RELEASE_NOTES'}
          ]
        }
      ],
    }
  }),
})
