---
home: true
title: INGAR Projects
heroImage: http://www.ingar.santafe-conicet.gov.ar/wp-content/uploads/2015/05/logo_ingar_365x50-2.png
heroHeight: 80
actions:
  - text: PUE Platform
    link: /en/plataforma/
    type: primary
  - text: App Starplastic
    link: /en/starplastic/
    type: secondary
  - text: App Green PtX
    link: /en/green_ptx/
    type: secondary
features:
- title: Productivity App
  details: Advanced productivity App platform through mathematical modeling
- title: Industry 4.0
  details: Industrial sensorization and production planning
- title: Energy Solutions
  details: Hydrogen leveling for wind farms
footer: Made in Santa Fe, Argentina | Copyright © 2024 INGAR
---